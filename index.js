// required file for require-directory.  Make it easier to setup routes to js files
const requireDirectory = require('require-directory');
module.exports = requireDirectory(module);
