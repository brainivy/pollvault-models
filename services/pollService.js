'use strict';
let debug = require('debug')('app:lib:pollservice'),
    colors = require('colors'),
    config = require('../../config/config'),
    cacheManager = require('./../../lib/cachemanager'),
    mongoose = require('mongoose'),
    PollModels = require('../poll'),
    inMemoryCacheSize = config.get('inMemoryCacheSize'),
    async = require('async'),
    _ = require('lodash'),
    AWS = require('aws-sdk'),
    pollService={};

const cache = require('../../lib/cachemanager').redisCache;
const environment = config.get('ENVIRONMENT');

function getPollQuestionDemos(pollID, questionID, populate, useCache=true){
    let response, question, data, distictResponse;
        return getPollQuestion(pollID, questionID, populate, useCache).then((question)=>{
            let responsePointer,
                returnPossibleDemos;
            question = question.toJSON();
            if (question.targetConfig.hasTargets){
                responsePointer = question.targetConfig.targets[0].responses[0];

            } else {
                responsePointer= question.responses[0];
            }

            returnPossibleDemos= responsePointer.groups.map(demoGroup=>{
                let newDemoGroup = demoGroup.demoValues.map(demoResponse=>{
                    delete demoResponse.demoValue;
                    delete demoResponse.demoPercent;// remove the field
                    return demoResponse
                })
                return demoGroup;
            })

            return returnPossibleDemos;
        });
}
function getPollByID(pollID, populate, isActive, useCache) {
    return getPoll(pollID, false, populate, isActive, useCache);
}
function getLatestPoll(populate, isActive, useCache) {
    return getPoll(null, true, populate, isActive, useCache);
}
function getPollWithoutQuestions(pollID, useCache) {
    return getPoll(pollID, false, false, useCache);
}

function getPoll(pollID, latest, populate=true, isActive, useCache=true) {
    let query={},
        p,
        pollExec,
        key = `pollvault:${environment}:poll:latest-${latest}:populate-${populate}`;  //default key

    query = {
        isActive : isActive,
        isRemoved : false
    };
    try {
        if (pollID) {
            query._id = mongoose.Types.ObjectId(pollID.trim());
            delete query.isActive;   //Since we are asking for a single poll...we dont care if its active or not
            key = `pollvault:${environment}:poll:pollID=${pollID}:populate-${populate}`;  //to get a single pollID
        }
    } catch (error) {
        return Promise.reject(error);
    };
    if (populate) {
        pollExec = PollModels.poll.findOne(query)
            .populate('questions.question')
            .sort({createdDateTime: -1});

    } else {
        pollExec = PollModels.poll.findOne(query, '-questions')

            .sort({createdDateTime: -1});
    }

    //TODO: Honor isActive on Question
    //TODO: Honor isActive on Demo/DemoGroup/Response
    return new Promise(function (fulfill, reject) {
        if (!useCache) {
            fulfill(pollExec.exec());
        }
        else {
            cache.wrap(key, function() {
                return pollExec.exec()
            }).then((poll=>{
                fulfill(poll)
            }))
        };
    });
}
function getPolls(includeNotActive, useCache){
    let query,
        p,
        pollExec,
        key = `pollvault:${environment}:poll:isActive=${includeNotActive}`;  //default key


    query = {
        isRemoved: false
    };

    if(!includeNotActive) {
        query.isActive = true;
    }

    debug('query: %s', JSON.stringify(query, null, 4).cyan)

    pollExec = PollModels.poll.find(query, '-questions')
        .lean(true)
        .sort({createdDateTime: -1});

    if (!useCache) {
        p = pollExec.exec();
    } else {
        p = cache.wrap(key, function(){
            return pollExec.exec();
        });
    }

    return p;
}
function getActivePolls(useCache){
    let query,
        p,
        pollExec,
        key = `pollvault:${environment}:poll:active`;  //default key

    query = {
        isActive : true,
        isRemoved : false
    };

    pollExec = PollModels.poll.find(query, '-questions')
        .sort({createdDateTime : -1});

    if(!useCache) {
        p = pollExec.exec();
    }
    else {
        p = cache.wrap(key, function(){
            return pollExec.exec();
        });
    }

    return p;
}
function getPollQuestionsDemoGroupResponse(pollID, questionID, demoGroup, useCache){
    return new Promise(function(fullfill, reject){
        getPollByID(pollID, useCache).then(poll=>{
            let question = _.filter(poll.questions,(question)=>{
                return question.question.questionID === questionID;
            });
            ///Now that we have the question...we need to filter out the demoGroup we are looking for-
            let newResponses=[];
            let flatReturn=[];

            question = _.head(question).question.toJSON();

            if (question.targetConfig.hasTargets){



            }
            question.responses.forEach(questionResponse =>{
                let filteredResponseGroup = _.remove(questionResponse.groups, {demoGroupName:demoGroup})
                filteredResponseGroup = _.head(filteredResponseGroup)
                filteredResponseGroup.demoResponses.forEach(response=>{
                    flatReturn.push({
                        pollID: pollID,
                        questionID: questionID,
                        demo: response.demoName,
                        demoGroup: filteredResponseGroup.demoGroupName,
                        responseText: questionResponse.name,
                        target: '',
                        targetID: '',
                        totalCount: response.value,
                        totalPercent: 0,
                        ordinal: 0
                        })
                    })
                    newResponses.push({
                        responseName: questionResponse.name,
                        groups: filteredResponseGroup
                    });
                });
                    fullfill(flatReturn);
            })
            .catch((error) => {
                reject(error);
            });
    });
}
function getPollQuestions(pollID, populate, isActive, useCache){
    let pollQuestions = [];
    return new Promise(function (fullfill, reject) {
        getPollByID(pollID, true, isActive,useCache).then((poll) => {
            poll.questions.forEach((question) => {
                let leanQuestion = useCache?question.question:question.question.toObject()
                    pollQuestions.push(hydrateQuestion(leanQuestion, populate));
            });
            fullfill(pollQuestions);
        }).catch(function (error) {
            reject(error);
        });
    });
}
function hydrateQuestion(question, populate){
    if (populate===false){
        delete question.terms;
        delete question.responses;
        question.targetConfig.targets.map((target)=>{
            delete target.terms;
            delete target.responses
            return target;
        })
    }
    return question;
}


function getPollQuestion(pollID, questionID, populate, useCache) {
    return new Promise(function (fullfill, reject) {

        //get the poll
        PollModels.poll.findById(pollID).then(poll => {

            let questionIds = poll.questions.map(q => {
                return q.question;
            });

            let query = {
                _id : { '$in' : questionIds},
                questionID : questionID
            }


            PollModels.question.findOne(query).then(question => {
                question = hydrateQuestion(question);
                fullfill(question);
            })
            .catch(error => {
                console.error(error);
                reject(error);
            })

        })
        .catch(error => {
            console.error(error);
            reject(error);
        })

        // getPollQuestions(pollID, populate,useCache).then((questions) => {
        //     let question = _.find(questions, {questionID: questionID});
        //     if (question){
        //         fullfill(question);
        //     } else {
        //         fullfill(null);
        //     }
        // }).catch(function (error) {
        //     reject(error);
        // });
    });
}

function getPollQuestionsPossibleResponses(pollID, questionID, target,useCache){
    return getPollQuestion(pollID, questionID, useCache).then((question)=>{
        let responsePointer
        if (question.targetConfig.hasTargets){
            responsePointer = question.targetConfig.targets[0].responses;

        } else {
            responsePointer= question.responses;
        }
        let possibleResponses= responsePointer.map((response)=>{
                return {
                    response: response.name,
                    ordinal: response.ordinal,
                    isActive: response.isActive
                }
        })
        return possibleResponses;
    });
}
function getPollQuestionsResponses(pollID, questionID, targetCD, useCache){
    return getPollQuestion(pollID, questionID, useCache).then((question)=> {
        let responsePointer
        if ((question.targetConfig.hasTargets) && (targetCD)) {
            responsePointer = _.find(question.targetConfig.targets, {targetCD: targetCD}).responses;
        } else {
            responsePointer = question.responses;
        }

        if (responsePointer) {
            return responsePointer;
        } else {
            return null;
        }
    });
}

function getPollQuestionsPossibleTargets(pollID, questionID, useCache=true){
    return getPollQuestion(pollID, questionID, useCache)
        .then((question)=>{
            let leanQuestion = useCache?question.toObject():question;
            let shrinkTargets= leanQuestion.targetConfig.targets.map((target)=> {
                delete target.terms;
                delete target.responses;
                return target;
            });
            question.targetConfig.targets = shrinkTargets;
            return question;
        })
        .catch((error)=>{
            return error;
        });
}

function updatePoll(pollID, pollUpdate) {
    var query = {_id: pollID },
        options = {
            upsert: false
        };
    return new Promise(function(fulfull, reject){
        PollModels.poll.findOneAndUpdate(query, pollUpdate, options).exec().then(function (updatedPoll){
            if (updatedPoll) {
                fulfull(updatedPoll);
            } else {
                reject();
            }
        });
    });
}
function updatePollQuestion(pollID, questionid, questionUpdate) {
    var query = {_id: questionid},
        options = {
            upsert: false,
            new : true
        };
    return new Promise(function (fulfull, reject) {
        PollModels.question.findOneAndUpdate(query, questionUpdate, options).exec().then(function (updatedQuestion) {
            if (updatedQuestion) {
                fulfull(updatedQuestion);
            } else {
                reject();
            }
        });
   });
}
function archivePoll(pollID, archiveFlag){
    debug(`Setting Archived Flag to ${archiveFlag} for poll- ${pollID}`);
    return PollModels.poll.findByIdAndUpdate({_id: pollID}, {$set: {isArchived: archiveFlag}}, {new: true, select: '-questions'}).exec();
}
function releasePoll(pollID, releaseFlag) {
    debug(`Setting Release Flag to ${releaseFlag} for poll- ${pollID}`);
    return PollModels.poll.findByIdAndUpdate({_id: pollID}, {$set: {isReleased: releaseFlag}}, {new: true, select: '-questions'}).exec();
}
function deletePoll(pollID, deleteFlag) {
    debug(`Setting isRemoved Flag to ${deleteFlag} for poll- ${pollID}`);
    return PollModels.poll.findByIdAndUpdate({_id: pollID}, {$set: {isRemoved: deleteFlag}}, {new: true, select: '-questions'}).exec();
}


pollService = {
    getPollByID: getPollByID,
    getLatestPoll: getLatestPoll,
    getActivePolls: getActivePolls,
    getPollWithoutQuestions: getPollWithoutQuestions,
    getPolls: getPolls,
    getPollQuestionDemos: getPollQuestionDemos,
    getPollQuestions: getPollQuestions,
    getPollQuestion: getPollQuestion,

    getPollQuestionsDemoGroupResponse: getPollQuestionsDemoGroupResponse,
    getPollQuestionsPossibleResponses: getPollQuestionsPossibleResponses,
    getPollQuestionsResponses: getPollQuestionsResponses,
    getPollQuestionsPossibleTargets: getPollQuestionsPossibleTargets,
    updatePollQuestion: updatePollQuestion,
    updatePoll: updatePoll,
    archivePoll: archivePoll,
    releasePoll: releasePoll,
    deletePoll: deletePoll
}


module.exports = pollService;
