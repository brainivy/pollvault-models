'use strict';
let mongoose = require('mongoose'),
    moment = require('moment'),
    extend = require('mongoose-schema-extend'), // eslint-disable-line
    baseSchema = require('./baseSchema.js');

let metaDataItemSchema = mongoose.Schema({
    name: {type: String, default: ''},                                 // name of metadata tag
    value: {type: String, default: ''}                                  // value of metadata tag
});

let reviewDataItemSchema = mongoose.Schema({
    username: {type: String, default: ''},                               // username of person who changed the state
    action: {type: String, default: ''},                                 // state Action that changed
    actionDate: {type: Date}                                             // date of Action
});

let demoValueSchema = mongoose.Schema({
    demoName: {type: String},
    ordinal: {type: Number, default: 0},
    value: {type: Number},
    percent: {type: Number},
    MOE: {type: Object},
    isActive: {type: Boolean, default: false},
}, {_id: false});

let demoGroupSchema = mongoose.Schema({
    demoGroupName: {type: String},
    ordinal: {type: Number, default: 0},
    isActive: {type: Boolean, default: false},
    demoValues: [demoValueSchema]
}, { _id: false});


let valueSchema = mongoose.Schema({
    name: {type: String},
    ordinal: {type: Number, default: 0},
    totalValue: {type: Number},
    totalPercent: {type: Number},
    MOE: {type: Object},
    isActive: {type: Boolean, default: false},

    groups: [demoGroupSchema]
}, {_id: false});

let targetSchema = mongoose.Schema({
    targetCD: {type: String},
    description: {type: String},
    keywords: [{type: String}],
    base: {type: String},
    isActive: {type: Boolean, default: false},
    sourceData: {
        formattedSource: {type: String},
    },
    responses: [valueSchema],
    terms: [valueSchema],
}, {_id: false});


let questionSchema = baseSchema.extend({
    questionID: {type: String},
    name: {type: String},
    text: {type: String},
    timeSeriesQuestionID: {type: String},
    targetConfig: {
        hasTargets: {type: Boolean, default: false},
        targets: [targetSchema]
    },
    base: {type: String},
    sourceData: {
        formattedSource: {type: String},
    },
    responses: [valueSchema],
    terms: [valueSchema],
    keywords: [{type: String}],
    datesConducted: {
        startDate: {type: Date},
        endDate: {type: Date}
    },
    publishData: [reviewDataItemSchema],
    metadata: [metaDataItemSchema],
    isReleased: {type: Boolean, default: false},
    isArchived: {type: Boolean, default: false}
});



let pollQuestionSchema = mongoose.Schema({
    question: {type: mongoose.Schema.Types.ObjectId, ref: 'question'},
});


let pollSchema = baseSchema.extend({
    originalSystemPollID: {type: String},
    sourceData: {
        source: {type: String},
        formattedSource: {type: String},
    },
    description: {type: String},
    stateConfig: {
        isStatePoll: {type: Boolean, default: false},
        stateCD: {type: String, default: ''}
    },
    datesConducted: {
        startDate: {type: Date},
        endDate: {type: Date}
    },
    uploadData: {
        storageTypeMetadata: {
            type: {type: String}
        },
        fileName: {type: String},
        uploadDate: {type: Date},
        uploadedUser: {type: String, default: ''},
    },
    processedData: {
        processedDate: {
            startDate: {type: Date},
            endDate: {type: Date}
        },
        isProcessed: {type: Boolean, default: false},
        errors: [{type: String}]
    },
    questions: [pollQuestionSchema],
    metadata: [metaDataItemSchema],
    publishData: [reviewDataItemSchema],
    isReleased: {type: Boolean, default: false},
    isArchived: {type: Boolean, default: false}
},
    {
        toObject: {virtuals: true },
        toJSON: {virtuals: true }
    }
);


pollSchema.virtual('datesConducted.readableDatesConducted').get(function () {
    if (moment(this.datesConducted.startDate).month() === moment(this.datesConducted.endDate).month()){
        return moment(this.datesConducted.startDate).format('MMMM D') + ' to ' + moment(this.datesConducted.endDate).format('D, YYYY');
    } else {
        return moment(this.datesConducted.startDate).format('MMMM D YYYY') + ' to ' + moment(this.datesConducted.endDate).format('MMMM D, YYYY');
    }
});


module.exports.poll = mongoose.model('poll', pollSchema, 'poll');
module.exports.question = mongoose.model('question', questionSchema, 'question');


module.exports.valueSchema = valueSchema;
module.exports.pollSchema = pollSchema;
module.exports.pollQuestionSchema = pollQuestionSchema;
module.exports.demoGroupSchema = demoGroupSchema;
module.exports.questionSchema = questionSchema;

