'use strict';
let mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend'), // eslint-disable-line
    baseSchema = require('./baseSchema.js');


let systemSettingsSchema = baseSchema.extend({
    name: {type: String, default: ''},                                 // name of metadata tag
    value: {type: Object}                                  // value of metadata tag
});



module.exports.systemSetting = mongoose.model('systemSetting', systemSettingsSchema, 'systemSetting');
module.exports.systemSettingSchema = systemSettingsSchema;
